<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama')-> nullable();
            $table->string('event')-> nullable();
            $table->integer('waktu')-> nullable();
            
            $table->string('alamat')-> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabel');
    }
}
