<?php

namespace App\Http\Controllers;

use App\Tabel;
use Illuminate\Http\Request;

class TabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tabels = Tabel::all();
        return view('tabel.index', compact('tabels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('tabel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> all();
    $new_tabel = new Tabel();
    $new_tabel-> name = $request ->nama;
    $new_tabel-> event = $request->event;
    $new_tabel-> waktu = $request->waktu;
    $new_tabel-> alamat = $request->alamat;
    $new_tabel-> save();

    return redirect() ->route('tabel.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = $tabel->id;
        $tabels = Tabel::find($id);
      //tampilkan data dengan item tertentu
        return view('tabel.show', compact('tabel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tabel $tabel)
    {
        return view('tabel.edit',compact('tabel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Tabel $tabel)
    {
        $tabel = Tabel::find($tabel->id);
        $tabel->name = $request->nama;
        $tabel->event = $request->event;
        $tabel->waktu = $request->waktu;
        $tabel->alamat = $request->alamat;
        $tabel->save();

        return redirect()->route('tabel.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tabel $tabel)
    {
        Tabel::destroy($tabel->id);
        return redirect()->route('tabel.index');
    }
}
