<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tabel extends Model
{
    protected $fillable = ['name', 'event', 'waktu', 'alamat'];
    protected $table = 'tabel';
}
