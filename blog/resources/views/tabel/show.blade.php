@extends('admin')

@section('content')
<br><br>
  <div class="container">
    <h2>Details</h2>
    <p>Nama : {{$tabel->nama}}</p>
    <p>Event : {{$tabel->event}}</p>
    <p>Waktu : {{$tabel->waktu}}</p>
    
    <p>Alamat : {{$tabel->alamat}}</p>
    <br>
    <a href="/admin/tabel" class="btn btn-success btn-sm">Back</a>
  </div>
@endsection