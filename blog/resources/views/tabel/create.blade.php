
@extends('admin')

@section('content')
<br><br>
<form action="{{ route ('tabel.index') }}" method="POST">
  {{ csrf_field() }}
 <div class="form-group">
   <label for="exampleInputEmail1">Nama</label>
   <input type="text" class="form-control" name="nama" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name">
 </div>
 <div class="form-group">
   <label for="exampleInputPassword1">Event</label>
   <input type="text" class="form-control" name="event" id="exampleInputPassword1" placeholder="Event Name">
 </div>
 <div class="form-group">
   <label for="exampleInputPassword1">Waktu</label>
   <input type="text" class="form-control" name="waktu" id="exampleInputPassword1" placeholder="Time">
 </div>
  <div class="form-group">
   <label for="exampleInputPassword1">Alamat</label>
   <input type="text" class="form-control" name="alamat" id="exampleInputPassword1" placeholder="Adress">
 </div>
 <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection