@extends('admin')

@section('content')
<table class="table">
  <thead>
    <tr>
    <a href="/admin/tabel/create" class="btn btn-primary btn-sm">Tambah Event</a>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Event</th>
      <th scope="col">Waktu</th>
      <th scope="col">Alamat</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  @foreach($tabels as $key => $tabel)
    <tr>
      <th scope="row">{{ $key + 1 }}</th>
      <td>{{ $tabel -> name }}</td>
      <td>{{ $tabel -> event }}</td>
      <td>{{ $tabel -> waktu }}</td>
      <td>{{ $tabel -> alamat }}</td>
      <td>
      <div class="row" >
        <a href="/admin/tabel/{{ $tabel->id }}" class="btn btn-info btn-sm">Show Details</a>
        <a href="/admin/tabel/{{ $tabel->id }}/edit" class="btn btn-warning btn-sm mx-2">Update</a>
        <br>
        <form action="/admin/tabel/{{ $tabel->id }}" method="post">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
        </form>
      
      </div>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection