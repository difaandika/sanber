@extends('admin')

@section('content')
<br><br>
<h2>Edit Page</h2>
<form action="/admin/tabel/{{$tabel->id}}" method="POST">
  {{ csrf_field() }}
  {{ method_field('PATCH') }}
 <div class="form-group">
   <label for="exampleInputEmail1">Nama</label>
   <input type="text" value="{{$tabel->name}}" class="form-control" name="nama" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
 </div>
 <div class="form-group">
   <label for="exampleInputPassword1">Event</label>
   <input type="text" value="{{$tabel->event}}" class="form-control" name="event" id="exampleInputPassword1" placeholder="Description">
 </div>
 <div class="form-group">
   <label for="exampleInputPassword1">Waktu</label>
   <input type="text" value="{{$tabel->waktu}}" class="form-control" name="waktu" id="exampleInputPassword1" placeholder="Price">
 </div>
 <div class="form-group">
   <label for="exampleInputPassword1">Alamat</label>
   <input type="text" value="{{$tabel->alamat}}" class="form-control" name="alamat" id="exampleInputPassword1" placeholder="Stock">
 </div>
 <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection